## Introduction
This project is managed on [Bitbucket](https://bitbucket.org/). This is repository shared craked [acunetix:13](https://www.acunetix.com/) software. 

## How to change username, password
You can change your username and password in the **install.expect** file.

## How to build up Image
first clone this project into you local.
```
$ git lfs clone https://voltwu@bitbucket.org/voltwu/acunetix-13-cracked.git
```
after your clone, you can build an image up from your local Dockerfile
```
$ cd acunetix-13-cracked
$ docker build --tag acunetix:13 .
```

## How to run the `acunetix:13` image
```
$ docker run -it --rm -p 5000:3443 acunetix:13
```
